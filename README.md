Hello Wunder Mobility,
-------------------------

As the technical requirements in the document say any third party library can be used and although I wasn't sure if that meant frameworks could be used I went ahead and used Yii2 since your job posting said Yii2 experience is a plus. 

Moreover Yii2 will take care of preventing SQL injections by using PDO prepared statements (plus I am also using utfmb4 as the charset), cross site scripting by automaticallyy sanitizing / escaping characters before they are displayed to the user and also cross site request forgeries using the CSRF Token that comes with every Yii2 form (however in this instance CSRF protection is Off since I extended the rest Controller to expose an api endpoint). I also added some dev tools for PHP code linting with php-cs but they are .gitignore-d


DATABASE
--------

In the folder named "database" you will find a .png file represending the Entity Relation Diagram for this project as well as the SQL produced by MySQL Workbench from that diagram. I went with splitting the data into multiple tables because it's certain that a user could have multiple addresses as well as multiple bank accounts.

The `personal` table is pretty straight forward (there are no username and password_hash fields as that was not specified as a requirement). The `address` table stores the address information and the `personal` table has an indentifying One to Many relation with it (since one user should be able to have more than one address). Identifying because there should never be an `address` row inserted without a coresponding `personal` row holding the user info. The `payment_data` table again is connected with an identifying One to Many relation. Properly designed databases that enforce restrictions on the data that's being inserted can prevent a lot potential points of failure as the project scales.

I also used the utf8mb4 character set and utf8mb4_unicode_ci collation and the InnoDB engine (which supports transactions).

There are 3 models which extend Active Record (the Yii2 Object relational mapper class). 

RESTful API and ORM
--------------------

My first thought was to create 3 separate REST controllers and simply expose all of the models via RESTful api endpoints however that would have meant either making an API call after each Step in the Vue SPA form or making successive calls to the API on the final Step in the Vue SPA. I chose instead to make a single call to the API posting all of the data (imagining having to work in a large application where limiting the number of API calls could save money and time). Further execution time gains could be made by inserting the data using mysql transactions, limiting the amount of time Model Lazy-Loading is used, using Model Eager-Loading where it makes sense to and eventually creating various Mysql Views to pull data in a query (where it could limit the total number of queries the app made to the database). Of course there are other means of improving script execution time like using the correct filtering, searching or sorting algorithms where applicable.

The PaymentController is responsible for taking in the POST request made to /payments and no other endpoints are exposed. Alternatively you could have a Model class extending the Yii2 Model class instead of ActiveRecord, representing and validating all of the data posted at once and then load that data into 3 ORM models extending the ActiveRecord class.

VUE FRONTEND
---------------

Beyond that I created a new Yii2 layout file to serve a Vue application called vue.php and also added a new VueAsset class comprised of 4 components which you will find in `src`. The benefit of creating a separate layout and asset class is that you don't serve Vue on every other page where it is not used.

Each component saves the step name to localStorage and the Vue router knows which component to route to so the user is able to start filling in the form and as long as he returns to the home page he will always be redirected to the last step he was on; after clicking Back to edit his previously submitted info he will be redirected to that step. Also all the data in each step is saved to localStorage when clicking Next. You could also save it on out of focus event or even key up event but I figured we should only save the data the user has chosen to save by clicking Next and not all of the data. There is also code linting with eslint.

QUESTIONS
----------

1. Describe possible performance optimizations for your Code.

A:

The Vue app runs on the client side and it's minified and bundled by webpack so it's tiny so performance-wise there isn't much more to do other than perhaps using a CDN (Content delivery network) because that way chances are the user already has the Vue library downloaded from other websites. 
I expressed some of the ways in which the Yii2 server-side app could be improved performance-wise in the paragraphs above. To that you can add keeping the application updated to the latest versions of MySQL, Apache2 and PHP; There are significant improvements between PHP 5.* and PHP 7 and also writing tests to maintaing the application integrity over the lifetime of the project.

2. Which things could be done better, than you�ve done it?

A:

There is a lot that could be done in terms of user experience and making sure data is validated on the frontend too, not just on the backend (Frontend validation wasn't a requirement but I wasn't going to build anything that doesn't have backend validation at the very least). Also it could be extended to handle errors and display meaningful error messages to the user whenever validation fails or there is some mishap on the server, especially when calling the various APIs. But typically you want to hide API error messages to the User and that's what I tried to do here.


UPDATE
----------

I wrapped the GuzzleHttp client inside another class and I registered it as a Singleton inside the Yii2 Dependency Injection Container with a configured GuzzleHttp client instance as its' dependency. You will find it under wmclient\WunderMobilityClient.php

By having WunderMobilityClient registered as a Singleton the same instance of the class can be accessed from the Yii2 Container instead of instantiating a new one. This would be a benefit if during the lifetime of the application multiple requests were made to the same API then you would always use the same Client class instance and you would only instantiate new Request objects holding the proper config and validating the parameters for each request and passing them to the client instance.

I tried to keep things very simple (just like the test) so the client consists of just a few methods; it's always difficult to uphold SOLID design principles in a small test challenge app because you're constantly thinking "this piece of code should be a class onto itself :) ". So in a perfect world I would have documentation for your API and I would build a complete client with request classes matching the resources exposed by the API. And one would be responsible for validating its' own data before the request was made. So please take into account that the solution matches the small size of the test challenge and code meant for production apps would be structured differently.

Best regards,


P.S.

You can find the app installed on my aws server at http://wm.swarmsoft.ro but if you would like to install the app locally please let me know.

1. git clone
2. composer install
3. create a mysql database called "wundermobility" (it's unfortunately hardcoded in the migration) along with a user identified by password (grant all on wundermobility.*), copy db.php to db-local.php and configure the connection details 
4. php yii migrate
5. php yii serve
6. navigate to http://localhost:8080 in your browser

By default the app is in dev; for production comment out the first 2 lines in web/index.php


UPDATE 2
----------

In all honesty I couldn't sleep last night thinking I didn't build a proper client class and request class so I woke up early today and did it. You will find the http client wrapper in the wmclient folder along with a base class that could be extended for all resource request classes in the future and the post paymentdata request class itself. There is also basic validation of all parameters and configurations and I am catching both client and server exceptions and logging them to the yi2 app log. Moreover the client is still registered as a singleton and passed to each new instance of the request class obtained via the dependency injection container. So, the client for calling the API is always the same instance during the lifetime of the app and a new request instance is created each time data is posted! :) So I am looking forward to sleeping better tonight.

UPDATE 03 August 2022
----------

1. I reinstalled this test app locally and found that I forgot to mention the mysql database name to be created locally in the Installation guide
2. I tried installing phpstan to run an analysis as there are many typehints and return type declarations missing however Yii2 was built on Php 5.6 and itself is missing property typehints and return type declarations and PhpStan stan can only be configured with a Php version as low as 7.1 and I can't typehint a property overridden in a child class when it was not typehinted in the parent class
3. Bottom line, I could not use phpstan to cleanup the code on short notice but normally I don't work without it; the php version in composer is 5.6 (as the Yii2 project added it) but I did use some typehints and return type declarations as well as other later php version features
