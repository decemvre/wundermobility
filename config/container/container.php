<?php

$definitions = require __DIR__ . "/definitions/definitions.php";
$singletons = require __DIR__ . "/singletons/singletons.php";

return [
    'definitions' => $definitions,
    'singletons' => $singletons,
];
