<?php

return [
    'PostWunderMobilityPaymentData' => function ($container, $params, $config) {
        $wmClient = $container->get('WunderMobilityClient');
        return new app\wmclient\resources\paymentdata\post\PaymentData($wmClient, $params, $config);
    },
];
