<?php

return [
    'WunderMobilityClient' => function ($container, $params, $config) {
        $clientConfig = [
            'baseUri' => Yii::$app->params['wunderMobility']['api']['baseUri'],
            'timeout' => Yii::$app->params['wunderMobility']['api']['timeout']
        ];
        return new app\wmclient\WunderMobilityClient($clientConfig);
    }
];
