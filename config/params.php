<?php

return [
    'wunderMobility' => [
        'api' => [
            'baseUri' => 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com',
            'timeout' => 5,
        ]
    ],
];
