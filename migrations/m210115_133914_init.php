<?php

use yii\db\Migration;

/**
 * Class m210115_133914_init
 */
class m210115_133914_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = file_get_contents(__DIR__.'/../database/wunderMobilityForwardEngineeringSQL.sql');
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210115_133914_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210115_133914_init cannot be reverted.\n";

        return false;
    }
    */
}
