import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import WunderMobility from './components/WunderMobility.vue'
import Personal from './components/Personal.vue'
import Address from './components/Address.vue'
import Payment from './components/Payment.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: WunderMobility,
      beforeEnter: (to, from, next) => {
        if (from.name === null && localStorage.step) {
          next({ name: localStorage.step })
        }
        next()
      }
    },
    {
      path: '/personal',
      name: 'personal',
      component: Personal
    },
    {
      path: '/address',
      name: 'address',
      component: Address,
      meta: {
        requiresPersonal: true
      }
    },
    {
      path: '/payment',
      name: 'payment',
      component: Payment,
      meta: {
        requiresPersonal: true,
        requiresAddress: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(rec => rec.meta.requiresPersonal)) {
    if (!localStorage.first_name ||
      !localStorage.last_name ||
      !localStorage.phone_number) {
      next({ name: 'personal' })
    }
  }
  if (to.matched.some(rec => rec.meta.requiresAddress)) {
    if (!localStorage.street ||
      !localStorage.house_number ||
      !localStorage.zipcode ||
      !localStorage.city) {
      next({ name: 'address' })
    }
  }
  next()
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
