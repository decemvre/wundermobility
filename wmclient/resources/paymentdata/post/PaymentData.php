<?php

declare(strict_types=1);

namespace app\wmclient\resources\paymentdata\post;

use app\wmclient\WunderMobilityClient as Client;
use app\wmclient\resources\Base;
use InvalidArgumentException;

class PaymentData extends Base
{
    private static $paramKeys = [
        'customerId',
        'iban',
        'owner',
    ];

    public function __construct(
        Client $wmClient,
        $params,
        $config = []
    ) {
        $this->validateParams($params);
        $this->wmClient = $wmClient;
        $this->method = 'POST';
        $this->path = 'default/wunderfleet-recruiting-backend-dev-save-payment-data';
        $this->prepareBody($params);
    }

    public function prepareBody($params): void
    {
        $filteredParams = array_intersect_key($params, array_flip(self::$paramKeys));
        $this->body = json_encode($filteredParams);
    }

    protected function validateParams($params): void
    {
        if (empty($params['customerId']) || empty($params['iban']) || empty($params['owner'])) {
            throw new InvalidArgumentException('Invalid parameters supplied to ' . get_class($this));
        }
    }
}
