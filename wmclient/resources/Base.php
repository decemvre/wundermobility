<?php

declare(strict_types=1);

namespace app\wmclient\resources;

use app\traits\ConfigurableTrait;
use GuzzleHttp\Psr7\Request as HttpRequest;
use InvalidArgumentException;

abstract class Base
{
    use ConfigurableTrait;

    public $wmClient;

    public $body;

    public $headers = [];

    public $path;

    public $method;

    public function __construct($config = [])
    {
        if (!empty($config)) {
            self::configure($this, $config);
        }
        $this->init();
    }

    public function init()
    {
        $this->validateRequestParams();
    }

    public function callService()
    {
        $request = new HttpRequest($this->method, $this->path, $this->headers, $this->body);
        return $this->wmClient->processRequest($request);
    }

    protected function validateRequestParams()
    {
        if (!isset($this->path) || empty($this->method)) {
            throw new InvalidArgumentException('Missing path or method for ' . get_class($this) . ' resource.');
        }
    }
}
