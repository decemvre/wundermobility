<?php

declare(strict_types=1);

namespace app\wmclient;

use Yii;
use InvalidArgumentException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Request as HttpRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Message;
use Psr\Http\Message\ResponseInterface;
use app\traits\ConfigurableTrait;

class WunderMobilityClient
{
    use ConfigurableTrait;

    public $httpClient;

    public $baseUri;

    public $timeout;

    public function __construct($config = [])
    {
        if (!empty($config)) {
            self::configure($this, $config);
        }
        $this->init();
    }

    public function init()
    {
        $this->validateClientConfiguration();

        $this->httpClient = new HttpClient([
            'base_uri' => $this->baseUri,
            'timeout' => 10,
        ]);
    }

    public function processRequest(HttpRequest $request): ?array
    {
        try {
            $response = $this->httpClient->send($request);
        } catch (ClientException | ServerException $e) {
            Yii::error(Message::toString($e->getRequest()));
            if ($e->hasResponse()) {
                Yii::error(Message::toString($e->getResponse()));
            }
        }
        if (isset($response) && ($response instanceof ResponseInterface) && ($response->getStatusCode() === 200)) { // should probably be 201 for resource created
            return $this->processResponse($response);
        } else {
            return null;
        }
    }

    public static function processResponse(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true);
    }

    private function validateClientConfiguration(): void
    {
        if (empty($this->baseUri)) {
            throw new InvalidArgumentException('Invalid $baseUri supplied to ' . get_class($this));
        }
    }
}
