<?php

declare(strict_types=1);

namespace app\traits;

trait ConfigurableTrait
{
    public static function configure(&$object, $config)
    {
        foreach ($config as $name => $value) {
            $object->$name = $value;
        }
    }
}
