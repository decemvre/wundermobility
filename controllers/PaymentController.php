<?php

declare(strict_types=1);

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use app\models\Personal;
use app\models\Address;
use app\models\Payment;

class PaymentController extends Controller
{
    public function actionCreate()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $personal = new Personal();
        $address = new Address();
        $payment = new Payment();
        if ($personal->load(Yii::$app->request->post()) &&
            $address->load(Yii::$app->request->post()) &&
            $payment->load(Yii::$app->request->post())
        ) {
            if ($personal->validate() &&
                $address->validate() &&
                $payment->validate() &&
                $personal->save()) {
                $personal->link('addresses', $address);
                if ($paymentData = $payment->exportPaymentInformation($personal)) {
                    Yii::$app->response->setStatusCode(201);
                    return [
                        'paymentDataId' => $paymentData->getPaymentData(),
                    ];
                } else {
                    Yii::$app->response->setStatusCode(500);
                    return [
                        'errorMessage' => "We could not process your request due to an Internal Server Error",
                    ];
                }
            } else {
                Yii::$app->response->setStatusCode(400);
                return [
                        'errorMessage' => "We could not validate your data.",
                    ];
            }
        }
    }

    /**
     * Declares the allowed HTTP verbs.
     * Please refer to [[VerbFilter::actions]] on how to declare the allowed verbs.
     * @return array the allowed HTTP verbs.
     */
    protected function verbs()
    {
        return [
            'create' => ['POST']
        ];
    }
}
