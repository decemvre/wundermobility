<?php

declare(strict_types=1);

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 *
 * @property Address[] $addresses
 * @property PaymentData[] $paymentDatas
 */
class Personal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'phone_number'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
        ];
    }

    /**
     * Gets query for [[Addresses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['personal_id' => 'id']);
    }

    /**
     * Gets query for [[PaymentDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentDatas()
    {
        return $this->hasMany(PaymentData::className(), ['personal_id' => 'id']);
    }

    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }
}
