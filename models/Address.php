<?php

declare(strict_types=1);

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property int $personal_id
 * @property string $street
 * @property string $house_number
 * @property string $zipcode
 * @property string $city
 *
 * @property Personal $personal
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['street', 'house_number', 'zipcode', 'city'], 'required'],
            [['personal_id'], 'integer'],
            [['street', 'city'], 'string', 'max' => 255],
            [['house_number'], 'string', 'max' => 10],
            [['zipcode'], 'string', 'max' => 45],
            [['personal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Personal::className(), 'targetAttribute' => ['personal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'personal_id' => 'Personal ID',
            'street' => 'Street',
            'house_number' => 'House Number',
            'zipcode' => 'Zipcode',
            'city' => 'City',
        ];
    }

    /**
     * Gets query for [[Personal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonal()
    {
        return $this->hasOne(Personal::className(), ['id' => 'personal_id']);
    }
}
