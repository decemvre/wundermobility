<?php

declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Personal;
use app\models\PaymentData;

/**
 *
 * @property string $account_owner
 * @property string $iban
 *
 */
class Payment extends Model
{
    public $account_owner;
    public $iban;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['account_owner', 'iban'], 'required'],
            [['account_owner', 'iban'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'account_owner' => 'Account Owner',
            'iban' => 'IBAN',
        ];
    }

    public function exportPaymentInformation(Personal $personal): ?PaymentData
    {
        // This belongs in a class matching the api endpoint
        $arrayData = [
            'customerId' => $personal->id,
            'iban' => $this->iban,
            'owner' => $personal->getFullName(),
        ];
        $postWunderMobilityPaymentData = Yii::$container->get('PostWunderMobilityPaymentData', $arrayData);
        $data = $postWunderMobilityPaymentData->callService();
        if (is_array($data) && array_key_exists('paymentDataId', $data)) {
            $paymentData = new PaymentData();
            $paymentData->payment_data = $data['paymentDataId'];
            if ($paymentData->validate()) {
                $personal->link('paymentDatas', $paymentData);
                return $paymentData;
            }
        }
        return null;
    }
}
