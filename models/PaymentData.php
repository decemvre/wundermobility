<?php

declare(strict_types=1);

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_data".
 *
 * @property int $id
 * @property int $personal_id
 * @property string $payment_data
 *
 * @property Personal $personal
 */
class PaymentData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_data'], 'required'],
            [['personal_id'], 'integer'],
            [['payment_data'], 'string', 'max' => 255],
            [['personal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Personal::className(), 'targetAttribute' => ['personal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'personal_id' => 'Personal ID',
            'payment_data' => 'Payment Data',
        ];
    }

    /**
     * Gets query for [[Personal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonal()
    {
        return $this->hasOne(Personal::className(), ['id' => 'personal_id']);
    }

    public function getPaymentData()
    {
        return $this->payment_data;
    }
}
